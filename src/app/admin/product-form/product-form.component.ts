import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../../services/category.service';
import {ProductService} from '../../services/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  private categories$: any[];

  constructor( private categoryService: CategoryService, private productService: ProductService) {}

  ngOnInit() {
    this.categoryService.getCategories().subscribe((categories) => {
      this.categories$ = categories;
    });
  }

  save(product){
    this.productService.create(product);
  }
}
